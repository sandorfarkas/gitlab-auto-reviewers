// ==UserScript==
// @name         GitLab auto reviewers
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  try to take over the world!
// @author       Sandor Farkas
// @match        https://gitlab.com/*/*/-/merge_requests/*/*
// @match        https://gitlab.com/*/*/-/merge_requests/new*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gitlab.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const backendReviewers = [
        'Roger Datafield',
        'Wendy Serverton',
        'Alan APIson',
    ];

    const frontendReviewers = [
        'Mark Webber',
        'Lily Styles',
        'Bella Browser',
    ];

    const AWAIT_TIME = 200;

    const holder = document.getElementsByClassName('merge-request-reviewer')[0];

    const generateClickFunction = (reviewers) => function(event) {
        event.stopPropagation();
        event.preventDefault();

        const dropdown = document.getElementsByClassName('js-reviewer-search')[0];
        dropdown.click();

        setTimeout(() => {
            reviewers.forEach((reviewer, index) => {
                setTimeout(() => {
                    const allUsers = [...document.getElementsByClassName('dropdown-menu-user-full-name')];

                    allUsers.filter(i => i.innerText.trim() === reviewer).forEach(i => i.click());
                }, (AWAIT_TIME * index) + AWAIT_TIME);
            });
        }, AWAIT_TIME);
    };

    const createButton = (repoName, reviewers, backgroundColor, textColor) => {
        const button = document.createElement("button");
        button.innerHTML = `Add ${repoName} reviewers`;
        button.style = `background: ${backgroundColor}; color: ${textColor}; margin: 1em;`;
        button.onclick = generateClickFunction(reviewers);
        return button;
    }
    const frontendReviewersButton = createButton('frontend', frontendReviewers, 'hotpink', 'white');
    const backendReviewersButton = createButton('backend', backendReviewers, 'lime', 'black');

    holder.appendChild(frontendReviewersButton);
    holder.appendChild(backendReviewersButton);
})();