# Gitlab auto reviewers

[Tampermonkey](https://www.tampermonkey.net/) script to have buttons in your merge requests that enables you to fill a set of reviewers automatically.

## Setup

Edit the file `gitlab-auto-reviewers.js` as described below and use it's content with Tampermonkey  

Create an array with the usernames (find examples in the code with frontendReviewers and backendReviewers) and add them at the bottom of the script:
eg: `holder.appendChild(frontendReviewersButton);`

## How it will look in Gitlab

![Gitlab auto reviewers](https://gitlab.com/sandorfarkas/gitlab-auto-reviewers/-/raw/main/auto-add-reviewer.png)
